#!/bin/bash -ex
#
source config.cfg

apt-get install -y qemu
apt-get install -y lvm2
apt-get install -y expect
apt-get install -y cinder-volume python-mysqldb

SECURE_MYSQL=$(expect -c "
set timeout 10
spawn sudo fdisk /dev/sdb
expect \"Command (m for help):\"
send \"n\r\"
expect \"Select (default p):\"
send \"p\r\"
expect \"Partition number (1-4, default 1):\"
send \"1\r\"
expect \"First sector ():\"
send \" \r\"
expect \"Last sector, +sectors or +size{K,M,G} ():\"
send \" \r\"
expect \"Command (m for help):\"
send \"t\r\"
expect \"Hex code (type L to list codes):\"
send \"8e\r\"
expect \"Command (m for help):\"
send \"p\r\"
expect \"Command (m for help):\"
send \"w\r\"
expect EOF
")
echo "$SECURE_MYSQL"

pvcreate /dev/sdb1
vgcreate cinder-volumes /dev/sdb1

sed  -r -e 's#(filter = )(\[ "a/\.\*/" \])#\1[ "a\/sda1\/", "a\/sdb\/", "r/\.\*\/"]#g' /etc/lvm/lvm.conf

filecinder=/etc/cinder/cinder.conf
rm $filecinder
cat << EOF > $filecinder
[DEFAULT]
rootwrap_config = /etc/cinder/rootwrap.conf
api_paste_confg = /etc/cinder/api-paste.ini
iscsi_helper = tgtadm
volume_name_template = volume-%s
volume_group = cinder-volumes
verbose = True
auth_strategy = keystone
state_path = /var/lib/cinder
lock_path = /var/lock/cinder
volumes_dir = /var/lib/cinder/volumes

rpc_backend = rabbit
my_ip = $BLOCK_MGNT_IP
enabled_backends = lvm

glance_host = controller

[database]
connection = mysql://cinder:$CINDER_DBPASS@$CON_MGNT_IP/cinder

[oslo_messaging_rabbit]
rabbit_host = controller
rabbit_userid = openstack
rabbit_password = $RABBIT_PASS

[oslo_concurrency]
lock_path =/var/lock/cinder

[keystone_authtoken]
auth_uri = http://$CON_MGNT_IP:5000
auth_url = http://$CON_MGNT_IP:35357
auth_plugin = password
project_domain_id = default
user_domain_id = default
project_name = service
username = cinder
password = $CINDER_PASS

[lvm]
volume_driver = cinder.volume.drivers.lvm.LVMVolumeDriver
volume_group = cinder-volumes
iscsi_protocol = iscsi
iscsi_helper = tgtadm

EOF

chown cinder:cinder $filecinder

service tgt restart
service cinder-volume restart

rm -f /var/lib/cinder/cinder.sqlite

echo "Finish"


