#!/bin/bash -ex
source config.cfg

ifaces=/etc/network/interfaces
rm $ifaces
touch $ifaces
cat << EOF >> $ifaces

auto lo
iface lo inet loopback

auto eth0
iface eth0 inet static
address $SWIFT_MGNT_IP
netmask $NETMASK_ADD

auto eth1
iface eth1 inet static
address $SWIFT_EXT_IP
netmask $NETMASK_ADD
gateway $GATEWAY_IP
dns-nameservers 8.8.8.8
EOF

echo "swift" > /etc/hostname
hostname -F /etc/hostname

iphost=/etc/hosts
rm $iphost
touch $iphost
cat << EOF >> $iphost
127.0.0.1       localhost
$CON_MGNT_IP    controller
$COM_MGNT_IP  	compute
$NET_MGNT_IP    network
$BLOCK_MGNT_IP  block
$SWIFT_MGNT_IP  swift
EOF

sleep 3
init 6