#!/bin/bash -ex
#
source config.cfg

apt-get update -y
apt-get upgrade -y
apt-get dist-upgrade -y

apt-get install -y ntp expect
apt-get install -y python-mysqldb 
apt-get install -y expect
apt-get install -y xfsprogs rsync

cp /etc/ntp.conf /etc/ntp.conf.bka
rm /etc/ntp.conf
cat /etc/ntp.conf.bka | grep -v ^# | grep -v ^$ >> /etc/ntp.conf
#
sed -i 's/server 0.ubuntu.pool.ntp.org/ \
#server 0.ubuntu.pool.ntp.org/g' /etc/ntp.conf

sed -i 's/server 1.ubuntu.pool.ntp.org/ \
#server 1.ubuntu.pool.ntp.org/g' /etc/ntp.conf

sed -i 's/server 2.ubuntu.pool.ntp.org/ \
#server 2.ubuntu.pool.ntp.org/g' /etc/ntp.conf

sed -i 's/server 3.ubuntu.pool.ntp.org/ \
#server 3.ubuntu.pool.ntp.org/g' /etc/ntp.conf

sed -i "s/server ntp.ubuntu.com/server $CON_MGNT_IP iburst/g" /etc/ntp.conf

service ntp restart

SECURE_MYSQL=$(expect -c "
set timeout 10
spawn sudo fdisk /dev/sdb
expect \"Command (m for help):\"
send \"n\r\"
expect \"Select (default p):\"
send \"p\r\"
expect \"Partition number (1-4, default 1):\"
send \"1\r\"
expect \"First sector ():\"
send \" \r\"
expect \"Last sector, +sectors or +size{K,M,G} ():\"
send \" \r\"
expect \"Command (m for help):\"
send \"t\r\"
expect \"Hex code (type L to list codes):\"
send \"8e\r\"
expect \"Command (m for help):\"
send \"p\r\"
expect \"Command (m for help):\"
send \"w\r\"
expect EOF
")
echo "$SECURE_MYSQL"

SECURE_MYSQL1=$(expect -c "
set timeout 10
spawn sudo fdisk /dev/sdc
expect \"Command (m for help):\"
send \"n\r\"
expect \"Select (default p):\"
send \"p\r\"
expect \"Partition number (1-4, default 1):\"
send \"1\r\"
expect \"First sector ():\"
send \" \r\"
expect \"Last sector, +sectors or +size{K,M,G} ():\"
send \" \r\"
expect \"Command (m for help):\"
send \"t\r\"
expect \"Hex code (type L to list codes):\"
send \"8e\r\"
expect \"Command (m for help):\"
send \"p\r\"
expect \"Command (m for help):\"
send \"w\r\"
expect EOF
")
echo "$SECURE_MYSQL1"

mkfs.xfs /dev/sdb1
mkfs.xfs /dev/sdc1

mkdir -p /srv/node/sdb1
mkdir -p /srv/node/sdc1

echo "/dev/sdb1 /srv/node/sdb1 xfs noatime,nodiratime,nobarrier,logbufs=8 0 2" >> /etc/fstab
echo "/dev/sdc1 /srv/node/sdc1 xfs noatime,nodiratime,nobarrier,logbufs=8 0 2" >> /etc/fstab

mount /srv/node/sdb1
mount /srv/node/sdc1


rsyncd=/etc/rsyncd.conf
rm $rsyncd
touch $rsyncd
cat << EOF > $rsyncd

uid = swift
gid = swift
log file = /var/log/rsyncd.log
pid file = /var/run/rsyncd.pid
address = $SWIFT_MGNT_IP
[account]
max connections = 2
path = /srv/node/
read only = false
lock file = /var/lock/account.lock
[container]
max connections = 2

path = /srv/node/
read only = false
lock file = /var/lock/container.lock
[object]
max connections = 2
path = /srv/node/
read only = false
lock file = /var/lock/object.lock

EOF

sed -i 's/RSYNC_ENABLE=false/ \
RSYNC_ENABLE=true/g' /etc/ntp.conf

service rsync start

apt-get -y install swift swift-account swift-container swift-object
curl -o /etc/swift/account-server.conf https://git.openstack.org/cgit/openstack/swift/plain/etc/account-server.conf-sample?h=stable/kilo
curl -o /etc/swift/container-server.conf https://git.openstack.org/cgit/openstack/swift/plain/etc/container-server.conf-sample?h=stable/kilo
curl -o /etc/swift/object-server.conf https://git.openstack.org/cgit/openstack/swift/plain/etc/object-server.conf-sample?h=stable/kilo
curl -o /etc/swift/container-reconciler.conf https://git.openstack.org/cgit/openstack/swift/plain/etc/container-reconciler.conf-sample?h=stable/kilo
curl -o /etc/swift/object-expirer.conf https://git.openstack.org/cgit/openstack/swift/plain/etc/object-expirer.conf-sample?h=stable/kilo

accountserver=/etc/swift/account-server.conf
rm $accountserver
touch $accountserver
cat << EOF > $accountserver
[DEFAULT]
bind_ip = $SWIFT_MGNT_IP
bind_port = 6002
user = swift
swift_dir = /etc/swift
devices = /srv/node

[pipeline:main]
pipeline = healthcheck recon account-server

[app:account-server]
use = egg:swift#account

[filter:healthcheck]
use = egg:swift#healthcheck

[filter:recon]
use = egg:swift#recon
recon_cache_path = /var/cache/swift

[account-replicator]

[account-auditor]

[account-reaper]

[filter:xprofile]
use = egg:swift#xprofile

EOF


containerserver=/etc/swift/container-server.conf
rm $containerserver
touch $containerserver
cat << EOF > $containerserver
[DEFAULT]
bind_ip = $SWIFT_MGNT_IP
bind_port = 6001
user = swift
swift_dir = /etc/swift
devices = /srv/node

[pipeline:main]
pipeline = healthcheck recon container-server

[app:container-server]
use = egg:swift#container

[filter:healthcheck]
use = egg:swift#healthcheck

[filter:recon]
use = egg:swift#recon
recon_cache_path = /var/cache/swift

[container-replicator]

[container-updater]

[container-auditor]

[container-sync]

[filter:xprofile]
use = egg:swift#xprofile

EOF

objectserver=/etc/swift/object-server.conf
rm $objectserver
touch $objectserver
cat << EOF > $objectserver
[DEFAULT]
bind_ip = $SWIFT_MGNT_IP
bind_port = 6000
user = swift
swift_dir = /etc/swift
devices = /srv/node

[pipeline:main]
pipeline = healthcheck recon object-server

[app:object-server]
use = egg:swift#object

[filter:healthcheck]
use = egg:swift#healthcheck

[filter:recon]
use = egg:swift#recon
recon_cache_path = /var/cache/swift
recon_lock_path = /var/lock

[object-replicator]

[object-reconstructor]

[object-updater]

[object-auditor]

[filter:xprofile]
use = egg:swift#xprofile

EOF

chown -R swift:swift /srv/node

mkdir -p /var/cache/swift
chown -R swift:swift /var/cache/swift

echo "RUN swift-init all start in swift node"


